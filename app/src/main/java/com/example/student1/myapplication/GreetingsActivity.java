package com.example.student1.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class GreetingsActivity extends AppCompatActivity {
    TextView tGreetings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greetings);
        tGreetings = (TextView) findViewById(R.id.tr);

        Intent intent = getIntent();
        String username = intent.getExtras().getString(MainActivity.USERNAME);
        Intent intent1 = getIntent();
        String hard = intent1.getExtras().getString("hard");
        if(MainActivity.cifri > 1 & MainActivity.cifri < 4  ) {
            tGreetings.setText("Сложность пароля = " + 1);
        }
        else if(MainActivity.cifri > 3 & MainActivity.cifri < 6){
            tGreetings.setText("Сложность пароля = " + 2 );
        }
        else if(MainActivity.cifri > 5){
            tGreetings.setText("Сложность пароля = " + 3 );
        }


    }
}

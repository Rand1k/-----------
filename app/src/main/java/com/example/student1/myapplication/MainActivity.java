package com.example.student1.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {
    EditText login;
    EditText password;
    EditText repassword;
    Button button;
    Button button1;
    Button button2;
    int b;
    SharedPreferences sPref;
    SharedPreferences sPref1;
    static String USERNAME;
    static String PASS;
   static int cifri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.but);
        button.setOnClickListener(this);
        button1 = (Button)findViewById(R.id.but2);
        button1.setOnClickListener(this);
        login = (EditText) findViewById(R.id.etl);
        password = (EditText) findViewById(R.id.etp);
        repassword = (EditText) findViewById(R.id.etr);


    }
    public void proverka(String a){
        char[] ca = a.toCharArray();

            for(b=0;b<ca.length-1;b++){
                boolean pp = (ca[b]<'a');
                if(pp == true){
                    ++cifri;
                    pp = false;
                }
            }
    }
    public void firstbutton (){
        String username = login.getText().toString();
        USERNAME = username;
        String pass = password.getText().toString();
        PASS = pass;
        String repass = repassword.getText().toString();
        Intent intent = new Intent(this, GreetingsActivity.class);
        intent.putExtra(USERNAME, username);
        Intent intent1 = new Intent(this, GreetingsActivity.class);
        intent1.putExtra("hard", cifri);
        sPref = getPreferences(MODE_PRIVATE);
        Editor ed = sPref.edit();
        ed.putString(USERNAME,pass);
        ed.commit();
        boolean a = repass.equals(pass);
        proverka(pass);
        if (a == false) {
            repassword.setError("Неправильно введен пароль");
        } else if (username.length() == 0) {
            login.setError("Поле не должно быть пустым");
        } else if (pass.length() < 6) {
            password.setError("Пароль должен быть больше 5 символов");
        } else if (cifri < 1) {
            password.setError("В пароле должно быть как минимум два символа");
            cifri = 0;
        } else startActivity(intent);
    }
    public void secondbutton(){
        sPref = getPreferences(MODE_PRIVATE);
        String savedText = sPref.getString(USERNAME,PASS);
        password.setText(savedText);
        Toast.makeText(this, "Пароль загружен", Toast.LENGTH_SHORT).show();
    }


    public void onClick(View view) {
        switch (view.getId()){
            case R.id.but:
                firstbutton();
                break;
            case R.id.but2:
                secondbutton();
                break;
        }

    }

}
